# GeoImg

GeoImg is a CLI-tool that can download Sentinel 2 images and make RGB composites.


## Getting Started
All shell commands presented here starts from project root (directory with this file).


### Prerequisites

* Ubuntu or other Linux distribution
* Python (expected versions from 3.10)


### Installing
* Clone repository using Git.

    ```
    git clone https://gitlab.com/max.rms/geoimg.git
    ```
* Change working directory to project root (directory with this `README.md` file).
  After that all commands should be executed from the project root.

    ```
    cd geoimg
    ```
* No installation required, the package can be run with `python -m` syntax.
  However, the package uses some external libraries, so we should prepare environment.
  Create virtualenv with app's dependencies using [`uw`](https://docs.astral.sh/uv/getting-started/installation/).

    ```
    uv sync
    ```

### Usage

  ```bash
  source .venv/bin/activate
  python -m geoimg search    # search products using search params
  python -m geoimg list      # list downloaded products
  python -m geoimg download  # search and download products using search params
  python -m geoimg rgb       # make RGB composites for each image in GI_IMG_DIR
  python -m geoimg    # show help with available commands
  ```

### Configuration
  Application reads settings from environment variables.
  Available configuration variables are:

  - `GI_PEPS_USER`: psername to access PEPS service (required for download)
  - `GI_PEPS_PASSWORD`: password to access PEPS service (required for download)
  - `GI_IMG_DIR`: directory to download images and create RGB composites (default is `~/geoimages/sentinel/`)
  - `GI_SEARCH_PARAMS_DIR`: direstory with json files with search params (default is `~/geoimages/search/`).

  Each file will be used by `search` and `download` commands to filter images
  on PEPS service (result is the union of search by each parameters object in each file).
  See [example of search params file](./examples/search.json).


  To set values one can use

  ```bash
  export GI_IMG_DIR=~/geoimages/sentinel/
  ```
  or create [.env file](https://docs.astral.sh/uv/configuration/files/#env):

  ```
  GI_PEPS_USER=john.joe@example.com
  GI_PEPS_PASSWORD=<password>
  GI_IMG_DIR=~/geoimages/sentinel/
  GI_SEARCH_PARAMS_DIR=~/geoimages/search/
  ```
  and launch download using

  ```bash
  uv run --env-file .env -- python -m geoimg download
  ```

