from pathlib import Path

import eodag
import pytest

from geoimg import exceptions, main


def test_list(img_dir, image_ids):
    products = main.list_products()
    assert set(image_ids) == set(products)


@pytest.mark.parametrize("remake", ["remake", "keep_existing"])
def test_make_rgb(remake, img_dir_with_rasters):
    remake = remake == "remake"

    main.make_rgb(remake_existed=remake)

    image_dirs = list(img_dir_with_rasters.iterdir())
    assert len(image_dirs) == 1
    image_dir = image_dirs[0]
    result, created = None, None
    files = [path for path in image_dir.iterdir() if path.is_file()]
    assert len(files) == 1
    result = files[0]
    assert result.suffix == ".tif"
    stat = result.stat()
    created = stat.st_ctime
    assert created == stat.st_mtime

    main.make_rgb(remake_existed=remake)

    new_stat = result.stat()
    if remake:
        assert new_stat.st_ctime > created
    else:
        assert new_stat.st_ctime == created


@pytest.fixture
def search_params() -> dict:
    return dict(
        productType="S2_MSI_L1C",
        geom={"lonmin": 1, "latmin": 43.5, "lonmax": 2, "latmax": 44},
        start="2021-01-01",
        end="2021-01-15",
    )


def test_search(mocker, search_params, search_params_dir):
    stub_result = eodag.SearchResult([])
    mocked_search = mocker.patch(
        "eodag.EODataAccessGateway.search_all",
        return_value=stub_result,
    )
    mocked_filter_property = mocker.patch(
        "eodag.SearchResult.filter_property",
        return_value=stub_result,
    )

    result = main.search(search_params_dir)

    mocked_search.assert_called_once_with(**search_params, provider="peps")
    mocked_filter_property.assert_called_once_with(cloudCover=90, operator="lt")
    assert result == {search_params_dir / "search.json": stub_result}


def test_download_check_envs(monkeypatch, search_params_dir, empty_img_dir):
    with pytest.raises(exceptions.ImproperlyConfigured):
        main.search(search_params_dir, download=True)
    monkeypatch.setenv("EODAG__PEPS__AUTH__CREDENTIALS__USERNAME", "user")
    monkeypatch.setenv("EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD", "")
    with pytest.raises(exceptions.ImproperlyConfigured):
        main.search(search_params_dir, download=True)


@pytest.mark.parametrize("with_rgb", [False, True], ids=["with_rgb", "no_rgb"])
def test_download(
    with_rgb,
    image_ids,
    peps_credentials,
    search_params_dir,
    empty_img_dir,
    mocker,
):
    new_image, existing_imges = image_ids[0], image_ids[1:]
    for image_id in existing_imges:
        (empty_img_dir / image_id).mkdir()
    assert not (empty_img_dir / new_image).exists()

    class FakeProduct:
        def __init__(self, image_id):
            self.properties = {"title": image_id}
            self._image_id = image_id
            self.downloaded = False

        def download(self):
            assert self._image_id == new_image, "download of existing image"
            self.downloaded = True
            downloaded_path = empty_img_dir / "tmp" / new_image
            downloaded_path.mkdir(parents=True)
            return str(downloaded_path)

    products = [FakeProduct(image_id) for image_id in image_ids]
    stub_result = eodag.SearchResult(products)
    mocked_search_all = mocker.patch(
        "eodag.EODataAccessGateway.search_all", return_value=stub_result
    )
    mocker.patch("eodag.SearchResult.filter_property", return_value=stub_result)
    mocked_make_rgb = mocker.patch("geoimg.rgb.make_rgb")

    main.search(search_params_dir, download=True, with_rgb=with_rgb)

    mocked_search_all.assert_called_once()
    new_product = products[0]
    assert new_product.downloaded is True
    assert (empty_img_dir / new_image).exists(), list(empty_img_dir.iterdir())
    assert list((empty_img_dir / "tmp").iterdir()) == []
    if with_rgb:
        mocked_make_rgb.assert_called_once_with(product_name=new_image)
    else:
        mocked_make_rgb.assert_not_called()


def _get_image_id(product):
    return product.properties["title"]
