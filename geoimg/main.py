import json
from pathlib import Path

import pydantic

from geoimg import conf, images, rgb
from geoimg.exceptions import IncorrectInput


def list_products():
    yield from rgb.list_products()


def make_rgb(remake_existed=False):
    rgb.make_rgbs(skip_existing=not remake_existed)


def search(search_params_path: Path, download=False, with_rgb=False):
    if download:
        conf.set_eodag_envs()
    path_to_search_params = _parse_params_path(search_params_path)
    search_results = {}
    for params_path, search_params in path_to_search_params.items():
        search_results[params_path] = images.search(
            search_params, download=download, with_rgb=with_rgb
        )
    return search_results


def _parse_params_path(params_path: Path):
    if not params_path.exists():
        raise IncorrectInput(f'Path "{params_path}" does not exist')

    files = []
    if params_path.is_file():
        files.append(params_path)
    elif params_path.is_dir():
        for path in params_path.iterdir():
            files.append(path)
        if not files:
            raise IncorrectInput(f'Directory "{params_path}" is empty')

    path_to_search_params = {}
    for path in files:
        search_params = _parse_params_file(path)
        path_to_search_params[path] = search_params
    return path_to_search_params


def _parse_params_file(params_path: Path):
    try:
        with open(params_path) as params_file:
            raw_search_params = json.load(params_file)
    except OSError as e:
        raise IncorrectInput(f"Cannot open file with search params: {e!r}")
    except json.JSONDecodeError as e:
        raise IncorrectInput(f"Cannot read json from file with search params: {e!r}")

    try:
        search_params = images.SearchParams.model_validate(raw_search_params)
    except pydantic.ValidationError as e:
        raise IncorrectInput(f"Incorrect search params in file {params_path}: {e!r}")

    return search_params
