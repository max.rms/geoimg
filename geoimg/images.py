import datetime
import logging
import typing as t
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

import eodag
from pydantic import BaseModel, Field

from geoimg import conf, exceptions, rgb


class Footprint(BaseModel):
    lonmin: int | float
    latmin: int | float
    lonmax: int | float
    latmax: int | float


class SearchParams(BaseModel):
    start: datetime.date
    end: datetime.date
    geom: Footprint
    max_cloud_cover: int = Field(alias="cloudCover")


def search(search_params, *, download=False, with_rgb=False):
    try:
        search_results = _search(search_params)
    except Exception as e:
        raise exceptions.ExternalLibraryException(f"Error during images search: {e}")
    if download:
        with ThreadPoolExecutor() as executor:
            for path in _download(search_results):
                if with_rgb:
                    executor.submit(rgb.make_rgb, product_name=path.name)
    return search_results


def _search(search_params) -> eodag.SearchResult:
    eodag.setup_logging(0, no_progress_bar=True)
    dag = eodag.EODataAccessGateway()
    dag.set_preferred_provider("peps")
    search_results = dag.search_all(
        productType="S2_MSI_L1C",
        geom=search_params.geom.model_dump(),
        start=search_params.start.isoformat(),
        end=search_params.end.isoformat(),
        provider="peps",
    ).filter_property(cloudCover=search_params.max_cloud_cover, operator="lt")
    return search_results


def _download(products: t.Iterable[eodag.EOProduct]):
    existed_products = set(rgb.list_products())
    for product in products:
        product_name = product.properties["title"]
        if product_name in existed_products:
            logger.info(f"Already have {product_name}")
            continue
        logger.info(f"Downloading {product_name}...")
        try:
            product_path_str = product.download()
        except Exception as e:
            logger.error(f"Error during downloading image {product_name}: {e}")
            continue
        if product_path_str.startswith("file:"):
            product_path_str = product_path_str.replace("file:", "")
        tmp_path = Path(product_path_str)
        assert tmp_path.exists(), tmp_path
        path = tmp_path.rename(conf.get().img_dir / tmp_path.name)
        logger.info(f"...downloaded as: {path}")
        yield path


logger = logging.getLogger(__name__)
