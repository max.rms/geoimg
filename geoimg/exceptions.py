class GeoimgException(Exception):
    pass


class ImproperlyConfigured(GeoimgException):
    """Raised when configuration is wrong."""


class IncorrectInput(GeoimgException):
    """Raised when input is wrong."""


class ExternalLibraryException(GeoimgException):
    """Raised when a third party library raised an exception."""
