import os
from pathlib import Path

import environ

from geoimg.exceptions import ImproperlyConfigured


@environ.config(prefix="GI")
class GeoImgConfig:
    img_dir = environ.var(
        "~/geoimages/sentinel/",
        converter=lambda dir: Path(dir).expanduser(),
        help="Directory with images and RGB composites",
    )
    search_params_dir = environ.var(
        "~/geoimages/search/",
        converter=lambda dir: Path(dir).expanduser(),
        help="Directory with JSON files with search params.",
    )

    @environ.config
    class Peps:
        user = environ.var("", help="Username to access PEPS service")
        password = environ.var("", help="Password to access PEPS service")

    @environ.config
    class Compression:
        """
        Options related to compression to pass to rasterio library.
        """

        enabled = environ.bool_var(True)
        compress = environ.var("DEFLATE", str)
        predictor = environ.var(2, int)
        zlevel = environ.var(9, int)

    peps = environ.group(Peps)
    compression = environ.group(Compression)


def get() -> GeoImgConfig:
    """
    Read configuration and store it.
    """
    return GeoImgConfig.from_environ(os.environ)


def set_eodag_envs():
    """
    Set eodag envs from config, raise if missed.
    """
    config = get()
    if not (config.peps.user and config.peps.password):
        raise ImproperlyConfigured(
            "Please set GI_PEPS_USER and GI_PEPS_PASSWORD environment variables."
        )
    os.environ["EODAG__PEPS__AUTH__CREDENTIALS__USERNAME"] = config.peps.user
    os.environ["EODAG__PEPS__AUTH__CREDENTIALS__PASSWORD"] = config.peps.password
